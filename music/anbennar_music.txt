
song = {
	name = "remnant_awakening_dwarves"

	chance = {
		modifier = {
			factor = 2
			has_country_modifier = dwarven_administration
		}	

		modifier = {
			factor = 0
			is_at_war = yes
		}
	}
}
song = {
	name = "maintheme"
	
	chance = {
		modifier = {
			factor = 0			#only played in mainscreen.
		}		
	}
}
song = {
	name = "dusk_of_an_empire"

	chance = {
		modifier = {
			factor = 0
			is_at_war = yes
		}
	}
}

song = {
	name = "in_the_wake"
	
	chance = {
		modifier = {
			factor = 0
			is_at_war = no
		}
		modifier = {
			factor = 1.5
			is_evil = yes
		}
		modifier = {
			factor = 1.5
			has_country_modifier = monstrous_nation
		}		
	}
}

song = {
    name = "Deioderan"
    
    chance = {
        modifier = {
            factor = 2
            is_at_war = yes
            OR = {
                religion = jadd
                capital_scope = { superregion = bulwar_superregion }
            }
        }    
        
        modifier = {
            factor = 0
            is_at_war = no
        }
        
        modifier = {
            factor = 0
            NOT = { capital_scope = { superregion = bulwar_superregion } }
            NOT = { religion = the_jadd }
        }
    }
}

song = {
	name = "dawn_of_an_empire_original"

	chance = {
		factor = 1.1
	}
}

song = {
	name = "strife_and_perseverance"
	
	chance = {
		modifier = {
            factor = 0
            NOT = {
				is_at_war = yes
			}
        }
		
		modifier = {
			factor = 2
			is_at_war = yes
		}
	}
}

song = {
	name = "sportless_hunt"
	
	chance = {
		modifier = {
            factor = 0
            NOT = {
				has_estate = estate_vampires
			}
        }
		
		modifier = {
			factor = 2
			has_estate = estate_vampires
		}
	}
}

song = {
	name = "adventurers_wanted_track"
	
	chance = {
		modifier = {
			factor = 1.5
			is_adventurer = yes
		}
		modifier = {
			factor = 1.5
			is_aelantiri_spawnable = yes
		}		
	}
}

song = {
	name = "the_unrelenting_wave"
	
	chance = {
		modifier = {
            factor = 0
            NOT = {any_owned_province = { continent = asia NOT = { superregion = forbidden_lands_superregion } } }
        }
		modifier = {
			factor = 1.5
			has_global_flag = rending_active
		}
		modifier = {
			factor = 2
			has_country_flag = rending_started_country
		}		
	}
}
